lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ssh_cert_parser/version'

Gem::Specification.new do |spec|
  spec.name          = 'ssh_cert_parser'
  spec.version       = SSHCertParser::VERSION
  spec.authors       = ['haseeb']
  spec.email         = ['haseebeqx@yahoo.com']

  spec.summary       = 'parses ssh certificates'
  spec.homepage      = 'https://gitlab.com/haseebeqx/ssh_cert_parser'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
