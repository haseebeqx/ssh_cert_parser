module SSHCertParser
  module PublicKeys
    class PublicKey
      def initialize(raw)
        @raw = raw
      end

      def fingerprint
        "SHA256:#{Digest::SHA256.base64digest(raw).partition("=")[0]}"
      end
    end
  end
end
