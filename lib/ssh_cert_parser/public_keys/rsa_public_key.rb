require 'ssh_cert_parser/public_keys/public_key'

module SSHCertParser
  module PublicKeys
    class RSAPublicKey < PublicKey
      attr_accessor :modulus, :exponent, :raw

      def initialize(modulus, exponent, raw = nil)
        @modulus = modulus
        @exponent = exponent
        @raw = raw
      end
    end
  end
end
