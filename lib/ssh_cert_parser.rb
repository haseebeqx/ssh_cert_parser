require 'ssh_cert_parser/version'
require 'ssh_cert_parser/ssh_certificate'

module SSHCertParser
  class << self
    def from_file(file)
      SSHCertificate.from_bytes(File.read(file))
    end

    def from_string(string)
      SSHCertificate.from_bytes(string)
    end
  end
end
