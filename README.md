# SSHCertParser

based on [https://github.com/EasyPost/ssh_certificate_parser](https://github.com/EasyPost/ssh_certificate_parser)

parses  [OpenSSH certificates](https://code.facebook.com/posts/365787980419535/scalable-and-secure-access-with-ssh/). it s capable of parsing both user certificates and host certificates. currently the following certifcate types are supported.

* ssh-rsa-cert-v01@openssh.com

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ssh_cert_parser'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ssh_cert_parser

## Usage

### from File

```ruby
cert = SSHCertParser.from_file('path/to/some/file')
cert.as_hash
```

### from string

```ruby
cert = SSHCertParser.from_string(File.read('path/to/some/file'))
cert.as_hash
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/haseebeqx/ssh_cert_parser. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the SSHCertParser project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/haseebeqx/ssh_cert_parser/blob/master/CODE_OF_CONDUCT.md).
