RSpec.describe SSHCertParser do
  it "has a version number" do
    expect(SSHCertParser::VERSION).not_to be nil
  end

  it 'parses certificate from file' do
    cert = SSHCertParser.from_file('spec/fixtures/id_rsa-cert.pub')

    expect(cert.as_hash[:key_id]).to eq('user_full_name')
  end

  it 'parses certificate from string' do
    cert = SSHCertParser.from_string(File.read('spec/fixtures/id_rsa-cert.pub'))

    expect(cert.as_hash[:key_id]).to eq('user_full_name')
  end
end
